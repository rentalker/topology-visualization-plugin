import { Color } from '@grafana/data';

export class Role {
  id: number;
  name: string;
  color: Color | string;
  expected: number;

  constructor(id = 0, name = 'default', color = 'orange', expected = 0) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.expected = expected;
  }
}

export function compareRoles(a: Role, b: Role) {
  if (a.id < b.id) {
    return -1;
  } else if (a.id > b.id) {
    return 1;
  }
  return 0;
}
