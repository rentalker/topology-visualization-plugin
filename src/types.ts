import { Role } from 'role';

export interface SimpleOptions {
  expectedRoles: Role[];
  toggleNodeLabels: boolean;
}
