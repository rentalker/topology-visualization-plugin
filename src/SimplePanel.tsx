import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { indexOfMax, renderErrorMessage } from './utility';
import { compareRoles } from 'role';
import { renderTopology, Topology, generateGraph } from 'topology';
import { brokenNode } from 'style';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  const [currentSlot, setCurrentSlot] = React.useState(0);
  const [topology, setTopology] = React.useState<Topology>({
    producer: brokenNode,
    representatives: [],
  });

  const expected_roles = options.expectedRoles.sort(compareRoles);

  /* Effect */
  React.useEffect((): any => {
    /* Get data */
    if (data.series.length === 0) {
      return renderErrorMessage('No Data');
    }
    let frame_clusters = data.series.find(value => value.refId === 'Clusters');
    console.log('Data: ', frame_clusters);
    if (frame_clusters === undefined) {
      return renderErrorMessage('Clusters query is missing');
    }

    const times: any = frame_clusters.fields.find(field => field.name === 'Time')!;
    let max_time_index = indexOfMax(times.values.buffer);

    const slots = frame_clusters.fields.find(field => field.name === 'slot');
    const names = frame_clusters.fields.find(field => field.name === 'node');
    const representatives = frame_clusters.fields.find(field => field.name === 'representative');
    const duties = frame_clusters.fields.find(field => field.name === 'duty');

    setCurrentSlot(slots?.values.get(max_time_index) - 1); //Warning: wo/ -1 might result in incomplete data
    setTopology(generateGraph(frame_clusters, names, representatives, slots, duties, currentSlot, expected_roles));
  }, [data, currentSlot, options, expected_roles]);

  console.log('ROLES: ', expected_roles);
  console.log('SLOT: ', currentSlot);

  return (
    <div id={'main'}>
      {console.log('TOPOLOGY: ', topology)}
      {renderTopology(
        topology.producer,
        topology.representatives,
        width,
        height,
        currentSlot,
        options.toggleNodeLabels
      )}
    </div>
  );
};
