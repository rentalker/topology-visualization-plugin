import React from 'react';
import { Role } from './role';
import { producerSize, representativeSize, baseNodeSize, brokenNode, borderOffset } from 'style';
import { renderErrorMessage, Position, createAngle, printLongName } from './utility';
import { BaseNodeCircle, BaseNodeCluster } from 'circle';
import { BaseLine } from 'line';

export interface BaseNode {
  id: string;
  hash: string;
  color: string;
  size: number;
}

export interface BaseCluster extends BaseNode {
  children: BaseNode[];
  strokeColor: string;
}

export interface BaseLink {
  source: string;
  target: string;
}

export interface Topology {
  producer: BaseCluster;
  representatives: BaseCluster[];
  // nodes: BaseNode[];
  // links: BaseLink[];
}

export function generateGraph(
  frame_clusters: any,
  names: any,
  representatives: any,
  slots: any,
  duties: any,
  slot: number,
  roles: Role[]
) {
  let producer: BaseCluster = brokenNode;
  let reps: BaseCluster[] = [];
  // let links: BaseLink[] = [];
  let found_producer = false;

  let bp_role: Role = roles.find(role => role.name === 'PRODUCER')!;
  let repr_role = roles.find(role => role.name === 'REPRESENTATIVE')!;

  for (let i = 0; i < frame_clusters.length; i++) {
    if (slots.values.get(i) === slot) {
      if (duties.values.get(i) === bp_role.name) {
        producer = {
          id: printLongName(names.values.get(i)),
          color: bp_role.color,
          hash: names.values.get(i),
          size: producerSize,
          children: [],
          strokeColor: repr_role.color,
        };
        if (found_producer) {
          renderErrorMessage('There are more than one BLOCK PRODUCER');
        }
        found_producer = true;
      }

      let temp_hash = representatives.values.get(i);
      if (!reps.find(repr => repr.hash === temp_hash)) {
        let role: Role = roles.find(role => role.name === duties.values.get(i))!;
        let temp_repr: BaseCluster = {
          id: printLongName(temp_hash),
          color: role.color,
          hash: temp_hash,
          size: representativeSize,
          children: [],
          strokeColor: repr_role.color,
        };
        reps.push(temp_repr);
      }

      // let temp_link: BaseLink = {
      //   source: printLongName(names.values.get(i)),
      //   target: printLongName(representatives.values.get(i)),
      // };
      // links.push(temp_link);
    }
  }

  if (producer === brokenNode) {
    renderErrorMessage('Network does not have a BLOCK PRODUCER');
  }

  for (let i = 0; i < frame_clusters.length; i++) {
    if (slots.values.get(i) === slot) {
      let role: Role = roles.find(role => role.name === duties.values.get(i))!;

      let temp_node: BaseNode = {
        id: printLongName(names.values.get(i)),
        color: role.color,
        hash: names?.values.get(i),
        size: baseNodeSize,
      };

      reps.find(repr => repr.hash === representatives.values.get(i))!.children.push(temp_node);
    }
  }

  reps = reps.filter(rep => rep.hash !== producer.hash);

  let topology: Topology = {
    producer: producer,
    representatives: reps,
  };
  return topology;
}

export function renderTopology(
  producer: BaseCluster,
  representatives: BaseCluster[],
  // links: BaseLink[],
  width: number,
  height: number,
  slot: number,
  label: boolean
): JSX.Element {
  const center = new Position(width / 2, height / 2);
  let border = Math.min(width, height) / 2 - borderOffset;

  let nodesCircleRadius_temp = (border - baseNodeSize * (1 + Math.sqrt(2))) / (1 + Math.sqrt(2));
  let reprCircleRadius = Math.sqrt(2) * (nodesCircleRadius_temp + baseNodeSize) + 2;

  const nodesCircleRadius = Math.min(
    ((Math.PI * reprCircleRadius) / representatives.length) * 0.9,
    nodesCircleRadius_temp
  );

  // console.log('NUMBERS: ', [border, nodesCircleRadius, reprCircleRadius]);
  const nodesTotalAngle = 270;

  let cluster_max_length = 0;
  representatives.forEach(representative => {
    if (cluster_max_length < representative.children.length) {
      cluster_max_length = representative.children.length;
    }
  });
  let nodeSize = Math.min(((Math.PI * nodesCircleRadius) / cluster_max_length) * 0.85, baseNodeSize);
  let clusterSize = nodeSize * 1.5;

  return (
    <svg
      id={'main'}
      width={width}
      height={height}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      // viewBox={'0 0 ' + width + ' ' + height}
    >
      <g id={'gmain'} key={'main' + slot}>
        {/* Nodes */}
        {representatives.map((representative: BaseCluster, rep_index) => {
          let repr_pos = createAngle(center, reprCircleRadius, rep_index, representatives.length);
          return representative.children.map((child: BaseNode, index) => {
            let node_pos = createAngle(
              repr_pos,
              nodesCircleRadius,
              index,
              representative.children.length,
              representatives.length,
              rep_index,
              nodesTotalAngle
            );
            return (
              <g key={index}>
                <BaseLine id={child.hash + representative.hash} start={node_pos} end={repr_pos} />
                <BaseNodeCircle node={child} position={node_pos} label={label} size={nodeSize} />
              </g>
            );
          });
        })}

        {/* Representatives */}
        {representatives.map((representative: BaseCluster, index) => {
          let repr_pos = createAngle(center, reprCircleRadius, index, representatives.length);
          // console.log('REPR POS: ', repr_pos);
          return (
            <g key={index}>
              <BaseLine id={representative.hash + producer.hash} start={repr_pos} end={center} />
              <BaseNodeCluster node={representative} position={repr_pos} label={label} size={clusterSize} />
            </g>
          );
        })}

        {/* Producer */}
        <BaseNodeCluster node={producer} position={center} label={label} />
      </g>
    </svg>
  );
}
