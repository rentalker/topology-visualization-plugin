import { PanelPlugin } from '@grafana/data';
import { SimpleOptions } from './types';
import { SimplePanel } from './SimplePanel';
import { SimpleEditor } from './SimpleEditor';
import { default_roles } from 'style';

export const plugin = new PanelPlugin<SimpleOptions>(SimplePanel).setPanelOptions(builder => {
  return builder
    .addCustomEditor({
      id: 'expectedRoles',
      path: 'expectedRoles',
      name: 'Expected Roles | Expected Number of Nodes',
      editor: SimpleEditor,
      defaultValue: default_roles,
    })
    .addBooleanSwitch({
      path: 'toggleNodeLabels',
      name: 'Toggle node labels display',
      defaultValue: true,
    });
});
