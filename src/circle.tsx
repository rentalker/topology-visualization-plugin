import React from 'react';
import { nodeIdLength, nodeLabelOffset, representativeStrokeWidth } from 'style';
import { BaseCluster, BaseNode } from './topology';
import { Position, printLongName } from './utility';

interface BaseNodeCircleProps {
  node: BaseNode;
  position: Position;
  label: boolean;
  size?: number;
}

interface BaseNodeClusterProps extends BaseNodeCircleProps {
  node: BaseCluster;
}

export function BaseNodeCircle({ node, position, label, size }: BaseNodeCircleProps): JSX.Element {
  return (
    <g>
      <circle id={node.hash} cx={position.x} cy={position.y} r={size ? size : node.size} fill={node.color} />
      {label && (
        <text
          id={'tc' + node.hash}
          x={position.x}
          y={position.y}
          fill="white"
          dx={node.size + nodeLabelOffset}
          dy=".35em"
          // fontSize={style.container_font_size}
          fontSize={node.size}
          fontWeight="normal"
        >
          {printLongName(node.id, nodeIdLength)}
        </text>
      )}
    </g>
  );
}

export function BaseNodeCluster({ node, position, label }: BaseNodeClusterProps): JSX.Element {
  return (
    <g>
      <circle
        id={node.hash}
        cx={position.x}
        cy={position.y}
        r={node.size}
        fill={node.color}
        stroke={node.strokeColor}
        strokeWidth={representativeStrokeWidth}
      />
      {label && (
        <text
          id={'tc' + node.hash}
          x={position.x}
          y={position.y}
          fill="white"
          dx={node.size + nodeLabelOffset}
          dy=".35em"
          // fontSize={style.container_font_size}
          fontSize={node.size} //default = 8
          fontWeight="normal"
        >
          {printLongName(node.id, nodeIdLength)}
        </text>
      )}
    </g>
  );
}
