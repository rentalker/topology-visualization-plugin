import { BaseCluster } from 'topology';
import { Role } from './role';

export const borderOffset = 10;

export const nodeIdLength = 5;
export const defaultNodeRadius = 20;
export const nodeLabelOffset = 1.5;

export const default_role_color = '#ab556f';
export const defaultLineColor = '#7ea9e1';

export const expected_table_bad_color = 'red';
export const expected_table_good_color = 'white';

export const representativeSize = 10;
export const producerSize = 15;
export const baseNodeSize = 8;

export const representativeStrokeWidth = 4;

export const brokenNode: BaseCluster = {
  id: 'broken',
  hash: 'broken',
  color: 'orange',
  size: baseNodeSize,
  strokeColor: 'red',
  children: [],
};

// now in main, this is for test_case only
export const default_roles = [
  new Role(0, 'PRODUCER', 'rgb(120, 237, 104)'),
  new Role(1, 'COMMITTEE', 'rgb(182, 109, 219)'),
  new Role(2, 'VALIDATOR', 'rgb(105, 158, 227)'),
  new Role(3, 'REPRESENTATIVE', 'rgb(222, 65, 84)'),
];
