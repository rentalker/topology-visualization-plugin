import React from 'react';
import { defaultLineColor } from './style';
import { Position } from './utility';

interface BaseLineProps {
  start: Position;
  end: Position;
  id: string;
}

export function BaseLine({ start, end, id }: BaseLineProps): JSX.Element {
  return (
    <line
      id={'li' + id}
      x1={start.x}
      y1={start.y}
      x2={end.x}
      y2={end.y}
      strokeWidth="2"
      stroke={defaultLineColor}
      opacity="0.8"
    />
  );
}
