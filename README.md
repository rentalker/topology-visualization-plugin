# P2P network topology visualization plugin for Grafana
[![pipeline status](https://gitlab.com/rentalker/topology-visualization-plugin/badges/master/pipeline.svg)](https://gitlab.com/rentalker/topology-visualization-plugin/-/commits/master)

P2P networks propagate information using gossip protocols. There are many variations of the general and implementation specifics but in general the family of protocols aims at gossiping the fact that new information is available in the network. Should a node hear about the gossip, and require the information it will contact neighbouring nodes asking for the data. In general, gossip protocols make no assumptions about the topology of the overlay network. However, with structured networks, the information exchange can be made much more efficient. The observed blockchain implementation utilized a semi structured network topology for propagating consensus based information. This is made possible by using a seed string shared between nodes that is used for pseudo-random role election every block. Using the seed, nodes self-elect into roles without the need to communicate. However, when performing roles, committee members must attest to the candidate block produced by the block producer. The seeded random is therefore also used to cluster the network using a k-means algorithm. The clustering is again performed by each node locally. The shared seed guarantees that nodes will produce the same topology, which is then used to efficiently propagate attestations to the block producer.

The network topology hence changes every slot. The plugin aims to visualize the changes in the network topology by drawing nodes, and their cluster representatives. Additionally, the consensus roles for each node are indicated with the vertex color. The following image shows the network plugin rendering a test network of 30 nodes real-time. The node in the center coloured green is the elected block producer for the current slot, nodes surrounded by the red stroke are cluster representatives, the rest of the nodes are coloured based on their role in the current slot, and connected to their cluster representative respectively.

![Topology_main](/uploads/ae74fd6dbe6ecec9f7577b66bbec6898/topology.jpg)

<!---
add screenshots
-->

## Installation

1. Download archieved plugin build from this project [Releases](https://gitlab.com/rentalker/topology-visualization-plugin/-/releases) and install in on your Grafana server:
   - Find the `plugins` property in the Grafana configuration file and set the `plugins` property to the path of the downloaded directory. Refer to the [Grafana configuration documentation](https://grafana.com/docs/grafana/v7.5/administration/configuration/) for more information.
   - [Restart Grafana service](https://grafana.com/docs/grafana/latest/installation/restart-grafana/)
2. Clone the repository and build the plugin (similar to the option **1**). For more information for the options **1** and **2** visit [Build a panel plugin](https://grafana.com/tutorials/build-a-panel-plugin/).
3. (Not available yet) Go to Grafana Plugins page and search for Topology plugin by Rentalker.

## Data preparation

All of the data should follow specific naming policy. There is one necessary query:
   - Clusters:
      ```sql
      SELECT "slot", "node", "duty", "representative" FROM "<table-name>" WHERE $timeFilter
      ```
      
   ![Clusters_query](/uploads/4eb35297a862ccbc6f5b9d00940533fb/clusters_query.png)

## Preparation and customization

After getting all the queries right, one should set up options menu in the Grafana's panel editor:
   - Roles (*REPRESENTATIVE* role is necessary):

   ![roles](/uploads/44dcd599ac16bcceca93dd4974ac9a16/options.png)

   If you know expected amount of nodes for some role, you can write it in the right cell to see this information in the plugin legend. If you don't need this information, leave those cells as 0's.

   - Slot time - time that is needed for the network to go to the next step (it is recommended to set your Grafana dashboard refresh time as a multiple of a *slot time* variable, usually *refresh time* == *slot time*):

   ![slotTime](/uploads/2867ddcee283b237c7e8c60131879687/slotTime.png)

## License
See the [License File](LICENSE).
